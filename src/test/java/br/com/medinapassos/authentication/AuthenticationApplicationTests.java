package br.com.medinapassos.authentication;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

@SpringBootTest
class AuthenticationApplicationTests {

    @Test
    void contextLoads() {
    }

    @Test
    void gerarPassword() {
        System.out.println((new BCryptPasswordEncoder()).encode("admin123@"));
    }

}
