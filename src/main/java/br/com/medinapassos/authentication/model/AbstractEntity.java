package br.com.medinapassos.authentication.model;

import java.io.Serializable;

public interface AbstractEntity extends Serializable {
    Long getId();
}
