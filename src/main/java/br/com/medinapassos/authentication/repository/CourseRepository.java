package br.com.medinapassos.authentication.repository;

import br.com.medinapassos.authentication.model.Course;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CourseRepository extends PagingAndSortingRepository<Course, Long> {
}
